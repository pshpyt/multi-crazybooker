
namespace :db do
  desc 'Also create hstore Schema'
  task :extensions => :environment  do
    # Create Schema
    ActiveRecord::Base.connection.execute 'CREATE SCHEMA  hstore;'
    # Enable Hstore
    ActiveRecord::Base.connection.execute 'CREATE EXTENSION  HSTORE SCHEMA hstore;'
    # Enable UUID-OSSP
    ActiveRecord::Base.connection.execute 'CREATE EXTENSION  "uuid-ossp" SCHEMA hstore;'
  end
end

Rake::Task["db:create"].enhance do
  Rake::Task["db:extensions"].invoke
end

Rake::Task["db:test:purge"].enhance do
  Rake::Task["db:extensions"].invoke
end
