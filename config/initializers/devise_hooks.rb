Warden::Manager.after_authentication do |user, _auth, _opts|
  user.create_activity :destroyed, parameters: { forename: user.forename, current_sign_in_ip: user.current_sign_in_ip, current_sign_in_at: user.current_sign_in_at }
end
