class SubdomainPresent
  def self.matches?(request)
    request.subdomain.present?
  end
end

class SubdomainBlank
  def self.matches?(request)
    request.subdomain.blank?
  end
end

Booker::Application.routes.draw do  
  resources :images
  resources :supplements
  mount Payola::Engine => '/payola', as: :payola
  
  default_url_options :host => "lvh.me:3000"
 # default_url_options :host => "sshrsa.com:3000"
  resources :steps
  get '/records/history', to: 'records#history', as: :records_history
  #resources :records
  resources :versions, only: [] do
    member do
      patch :bringback  # <= and that
    end
  end
  
  resources :records do
    collection do 
      get :deleted # <= this
    end
    member do
     get :update_field, to: 'records#update_field' 
    end
    
    resources :versions, only: [:destroy] do
      member do
        get :diff, to: 'versions#diff'
        patch :rollback, to: 'versions#rollback'
      end
    end

    collection do
      get   :manage
      get   :drafts
      get   :awaiting_approvement
      get   :approved
      get   :approvement_request
      get   :accept_approvement
      get   :cancel_approvement
      # required for Sortable GUI server side actions
      post :rebuild
    end
  end
  resources :inputs
  resources :attachments
  resources :forms
  resources :former
  get "home", to: "pages#home", as: "home"  
  match "create_record", to: "records#create_record", as: "create_record", via: [:get, :post]
  match "import_csv", to: "records#import_csv", as: "import_csv", via: [:post]
  get "posts", to: "pages#posts", as: "posts"
  get "posts/:id", to: "pages#show_post", as: "post"
  devise_for :users

  constraints(SubdomainPresent) do    
    root "pages#inside", as: :dashboard
    get "inside", to: "pages#inside", as: "inside"    
    get "activity", to: "pages#activity", as: "activity"    
    
    post 'qrc/disable_otp'
    post 'qrc/enable_otp'

    namespace :admin do
      root "base#index"
      post "users/invite_user", as: 'invite_user'
      resources :users
      get "posts/drafts", to: "posts#drafts", as: "posts_drafts"
      get "posts/dashboard", to: "posts#dashboard", as: "posts_dashboard"
      get "forms/dashboard", to: "forms#dashboard", as: "forms_dashboard"
      resources :posts
      resources :forms
      resources :inputs      
    end
  end

  constraints(SubdomainBlank) do
    post 'pages/require_subdomain'
    root "pages#home"
    resources :accounts, only: [:new, :create]
  end

end
