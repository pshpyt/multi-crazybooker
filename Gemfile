source 'http://rubygems.org'
# ruby '2.2.2'

# Standard Rails gems
gem 'rails', '4.2.2'
gem 'sass-rails', '5.0.3'
gem 'uglifier', '2.7.1'
gem 'coffee-rails', '4.1.0'
gem 'jquery-rails', '4.0.4'
gem 'awesome_print'
gem 'xml-simple'
# gem 'turbolinks', '2.5.3'
gem 'jbuilder', '2.3.0'
gem 'bcrypt', '3.1.10'
gem 'nested_form'
gem 'wicked'
gem 'hstore_accessor'
gem 'signature-pad-rails'
gem 'paperclip-dropbox', '>= 1.1.7'
gem 'paperclip'
gem 'simple_form'
gem 'bootstrap-datepicker-rails'
gem 'smarter_csv'
gem 'paper_trail', '~> 4.0.0.rc'
gem 'responders'
gem 'awesome_nested_set' # or any similar gem (gem 'nested_set')
gem 'the_sortable_tree', '~> 2.5.0'
gem 'jquery-ui-rails'
gem 'haml'
gem 'bootstrap-tagsinput-rails'
gem 'twitter-typeahead-rails'
gem 'breadcrumbs_on_rails'
gem 'rmagick'
# gem 'thinreports-rails'
gem 'thinreports', '0.8.0'
gem 'remotipart'
gem 'ransack'
gem 'cancancan', '~> 1.10'
gem 'has_scope'
gem 'messengerjs-rails', '~> 1.4.1'
gem 'jquery-turbolinks'
gem 'diffy'
gem 'nprogress-rails'
gem 'counter_culture', '~> 0.1.33'
gem 'slim'
gem 'autoprefixer-rails'
gem 'auto_breadcrumbs'
gem 'country_select'
gem 'gmaps4rails'
gem 'geocoder'
gem 'dependent-fields-rails'
# gem 'geocoder-hstore', git: 'https://github.com/unite4good/geocoder-hstore.git'
gem 'typeahead-addresspicker-rails'
# gem 'fastimage'
# gem "parse-ruby-client"
gem 'parse-ruby-client', git: 'https://github.com/adelevie/parse-ruby-client.git'
# Necessary for Windows OS (won't install on *nix systems)
gem 'tzinfo-data', platforms: [:mingw, :mswin]

# Kaminari: https://github.com/amatsuda/kaminari
gem 'kaminari', '0.16.3'

# Friendly_id: https://github.com/norman/friendly_id
gem 'friendly_id', '5.1.0'

# Font-awesome: https://github.com/FortAwesome/font-awesome-sass
gem 'font-awesome-sass', '4.2.0'
gem 'public_activity'
gem 'simple-line-icons-rails'
gem 'sidekiq'
gem 'apartment-sidekiq'
# RailsBricks: http://www.railsbricks.net
# gem 'railsbricks'
# Bootstrap 3: https://github.com/twbs/bootstrap-sass
gem 'bootstrap-sass', '3.3.5'
# gem 'prawn'
group :development, :test, :production do
  gem 'byebug', '5.0.0'
  gem 'web-console', '2.1.2'
  # Figaro: https://github.com/laserlemon/figaro
  gem 'figaro', '1.1.1'
  gem 'thin'
  # Spring: https://github.com/rails/spring
  gem 'spring', '1.3.6'
  gem 'pry'
  # Annotate_Models: https://github.com/ctran/annotate_models
  gem 'annotate', '2.6.10'
  gem 'rails-erd'
  gem 'letter_opener'
  gem 'capistrano'
  gem 'bullet'
  gem 'rack-mini-profiler'
  gem 'metric_fu'
  gem 'brakeman'
  gem 'ruby-growl'
  gem 'latent_object_detector'
  gem 'lol_dba'
  gem 'rubocop'
  gem 'rubycritic', :require => false
end

gem 'apartment'

# gem 'prawn-rails'
# PostgreSQL
gem 'pg'

# Devise: https://github.com/plataformatec/devise
gem 'devise', '3.5.1'
gem 'devise_invitable'
gem 'devise-two-factor', '~> 1.0.0'
gem 'rqrcode-rails3'
gem 'mini_magick'
gem 'sprockets-rails', require: 'sprockets/railtie'
gem 'payola-payments'

# Redcarpet: https://github.com/vmg/redcarpet
gem 'redcarpet', '3.3.1'

# Rails 12factor for Heroku: https://github.com/heroku/rails_12factor
group :production do
  gem 'rails_12factor'
end

# Unicorn: http://unicorn.bogomips.org
group :production do
  gem 'unicorn'
end
