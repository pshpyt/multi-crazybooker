class AddFirstNameToAccounts < ActiveRecord::Migration
  def change
    add_column :accounts, :first_name, :string
    add_column :accounts, :last_name, :string
    add_column :accounts, :address_1, :text
    add_column :accounts, :address_2, :text
    add_column :accounts, :city, :string
    add_column :accounts, :state, :string
    add_column :accounts, :country, :string
    add_column :accounts, :main_phone, :string
    add_column :accounts, :company_name, :string
    add_column :accounts, :industry, :string
    add_column :accounts, :company_size, :string
    add_column :accounts, :mobile_phone, :string
    add_column :accounts, :credit_card, :string
    add_column :accounts, :address_same_as_physical, :boolean
    add_column :accounts, :name_on_card, :string
    add_column :accounts, :ccv, :string
    add_column :accounts, :billing_address_1, :text
    add_column :accounts, :billing_address_2, :text
    add_column :accounts, :billing_city, :string
    add_column :accounts, :billing_state, :string
    add_column :accounts, :billing_country, :string
    add_column :accounts, :extras, :hstore
  end
end
