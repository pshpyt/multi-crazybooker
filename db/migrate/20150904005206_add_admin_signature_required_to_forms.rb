class AddAdminSignatureRequiredToForms < ActiveRecord::Migration
  def change
    add_column :forms, :admin_signature_required, :boolean
  end
end
