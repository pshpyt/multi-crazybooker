class CreateForms < ActiveRecord::Migration
  def change
    create_table :forms do |t|
      t.string :name
      t.text :description
      t.text :content_md
      t.text :content_html      
      t.string :slug
      t.integer :account_id

      t.timestamps
    end
  end
end
