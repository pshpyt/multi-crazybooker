class AddHelpToInputs < ActiveRecord::Migration
  def change
    add_column :inputs, :help, :text
  end
end
