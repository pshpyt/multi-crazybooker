class CreateLocations < ActiveRecord::Migration
  def change
    create_table :locations do |t|
      t.string :full_address
      t.float :latitude
      t.float :longitude
      t.references :mapable, polymorphic: true, index: true
      t.hstore :data

      t.timestamps null: false
    end
  end
end
