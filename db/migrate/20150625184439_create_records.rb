class CreateRecords < ActiveRecord::Migration
  def change
    create_table :records do |t|
      t.integer :form_id
      t.hstore :data

      t.timestamps null: false
    end
  end
end
