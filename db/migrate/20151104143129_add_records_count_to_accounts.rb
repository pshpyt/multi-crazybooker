class AddRecordsCountToAccounts < ActiveRecord::Migration

  def self.up

    add_column :accounts, :records_count, :integer, :null => false, :default => 0

  end

  def self.down

    remove_column :accounts, :records_count

  end

end
