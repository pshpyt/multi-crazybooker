class AddRecordsCountToForms < ActiveRecord::Migration

  def self.up

    add_column :forms, :records_count, :integer, :null => false, :default => 0

  end

  def self.down

    remove_column :forms, :records_count

  end

end
