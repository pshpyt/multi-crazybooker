class AddSlugToInputs < ActiveRecord::Migration
  def change
    add_column :inputs, :slug, :string
    add_index :inputs, :slug, unique: true
  end
end
