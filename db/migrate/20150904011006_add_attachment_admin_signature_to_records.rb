class AddAttachmentAdminSignatureToRecords < ActiveRecord::Migration
  def self.up
    change_table :records do |t|
      t.attachment :admin_signature
    end
  end

  def self.down
    remove_attachment :records, :admin_signature
  end
end
