class AddSignatureRequiredToForms < ActiveRecord::Migration
  def change
    add_column :forms, :signature_required, :boolean
  end
end
