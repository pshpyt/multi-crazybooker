class AddStepIdToInputs < ActiveRecord::Migration
  def change
    add_column :inputs, :step_id, :integer
  end
end
