class CreateSteps < ActiveRecord::Migration
  def change
    create_table :steps do |t|
      t.integer :number
      t.string :title
      t.text :description
      t.integer :form_id

      t.timestamps null: false
    end
  end
end
