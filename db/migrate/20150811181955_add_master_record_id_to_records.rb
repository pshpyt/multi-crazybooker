class AddMasterRecordIdToRecords < ActiveRecord::Migration
  def change
    add_column :records, :master_record_id, :integer
  end
end
