class CreateInputs < ActiveRecord::Migration
  def change
    create_table :inputs do |t|
      t.string :name
      t.text :description
      t.boolean :required
      t.integer :form_id

      t.timestamps null: false
    end
  end
end
