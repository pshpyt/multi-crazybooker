class AddFileUploadToForms < ActiveRecord::Migration
  def change
    add_column :forms, :file_upload, :boolean
  end
end
