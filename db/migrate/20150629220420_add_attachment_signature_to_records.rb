class AddAttachmentSignatureToRecords < ActiveRecord::Migration
  def self.up
    change_table :records do |t|
      t.attachment :signature
    end
  end

  def self.down
    remove_attachment :records, :signature
  end
end
