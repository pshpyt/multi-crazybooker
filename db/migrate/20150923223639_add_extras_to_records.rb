class AddExtrasToRecords < ActiveRecord::Migration
  def change
    add_column :records, :extras, :hstore
  end
end
