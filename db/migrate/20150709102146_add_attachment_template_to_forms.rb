class AddAttachmentTemplateToForms < ActiveRecord::Migration
  def self.up
    change_table :forms do |t|
      t.attachment :template
    end
  end

  def self.down
    remove_attachment :forms, :template
  end
end
