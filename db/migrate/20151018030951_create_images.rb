class CreateImages < ActiveRecord::Migration
  def change
    create_table :images do |t|
      t.integer :supplement_id

      t.timestamps null: false
    end
  end
end
