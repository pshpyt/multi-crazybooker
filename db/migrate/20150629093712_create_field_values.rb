class CreateFieldValues < ActiveRecord::Migration
  def change
    create_table :field_values do |t|
      t.integer :input_id
      t.string :value

      t.timestamps null: false
    end
  end
end
