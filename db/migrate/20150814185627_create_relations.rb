class CreateRelations < ActiveRecord::Migration
  def change
    create_table :relations do |t|
      t.integer :record_id
      t.integer :related_record_id

      t.timestamps null: false
    end
  end
end
