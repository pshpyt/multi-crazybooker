class AddDataToInputs < ActiveRecord::Migration
  def change
    add_column :inputs, :data, :hstore
  end
end
