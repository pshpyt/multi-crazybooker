class AddMasterFormIdToForms < ActiveRecord::Migration
  def change
    add_column :forms, :master_form_id, :integer
  end
end
