class AddNoToInputs < ActiveRecord::Migration
  def change
    add_column :inputs, :no, :string
    add_index :inputs, :no, unique: true
  end
end
