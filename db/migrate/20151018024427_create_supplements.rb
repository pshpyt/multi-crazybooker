class CreateSupplements < ActiveRecord::Migration
  def change
    create_table :supplements do |t|
      t.string :title
      t.string :slug
      t.text :description
      t.integer :images_count, :integer, :default => 0

      t.timestamps null: false
    end
    add_index :supplements, :slug, unique: true
  end
end
