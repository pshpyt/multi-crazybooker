# encoding: utf-8
class AddMissingIndexes < ActiveRecord::Migration
      def change
        
        #add_index :activities, [:recipient_id, :recipient_type]
        add_index :users, [:invited_by_id, :invited_by_type]
        add_index :attachments, :record_id
        add_index :field_values, :input_id
        add_index :forms, :master_form_id
        add_index :forms, :account_id
        add_index :images, :supplement_id
        add_index :inputs, :form_id
        add_index :inputs, :step_id
        add_index :locations, [:mapable_id, :mapable_type]
        add_index :relations, :record_id
        add_index :relations, :related_record_id
        add_index :relations, [:record_id, :related_record_id]
        add_index :records, :parent_id
        add_index :records, :master_record_id
        add_index :records, :form_id
        add_index :records, :user_id
        add_index :steps, :form_id
      end
    end

