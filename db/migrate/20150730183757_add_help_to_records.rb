class AddHelpToRecords < ActiveRecord::Migration
  def change
    add_column :records, :help, :text
  end
end
