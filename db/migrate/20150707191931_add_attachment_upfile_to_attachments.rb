class AddAttachmentUpfileToAttachments < ActiveRecord::Migration
  def self.up
    change_table :attachments do |t|
      t.attachment :upfile
    end
  end

  def self.down
    remove_attachment :attachments, :upfile
  end
end
