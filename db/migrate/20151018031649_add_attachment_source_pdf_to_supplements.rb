class AddAttachmentSourcePdfToSupplements < ActiveRecord::Migration
  def self.up
    change_table :supplements do |t|
      t.attachment :source_pdf
    end
  end

  def self.down
    remove_attachment :supplements, :source_pdf
  end
end
