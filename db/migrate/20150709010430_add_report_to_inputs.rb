class AddReportToInputs < ActiveRecord::Migration
  def change
    add_column :inputs, :report, :boolean
  end
end
