class AddParentIdToRecords < ActiveRecord::Migration
  def self.up
    add_column :records, :parent_id, :integer # Comment this line if your project already has this column
    # Category.where(parent_id: 0).update_all(parent_id: nil) # Uncomment this line if your project already has :parent_id
    add_column :records, :lft,       :integer
    add_column :records, :rgt,       :integer

    # optional fields
    add_column :records, :depth,          :integer
    add_column :records, :children_count, :integer

    # This is necessary to update :lft and :rgt columns
    #Record.rebuild!
  end

  def self.down
    remove_column :records, :parent_id
    remove_column :records, :lft
    remove_column :records, :rgt

    # optional fields
    remove_column :records, :depth
    remove_column :records, :children_count
  end
end
