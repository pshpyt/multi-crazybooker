// This is a manifest file that'll be compiled into application.js, which will include all the files
// listed below.
//
// Any JavaScript/Coffee file within this directory, lib/assets/javascripts, vendor/assets/javascripts,
// or vendor/assets/javascripts of plugins, if any, can be referenced here using a relative path.
//
// It's not advisable to add code directly here, but if you do, it'll appear at the bottom of the
// compiled file.
//
// Read Sprockets README (https://github.com/sstephenson/sprockets#sprockets-directives) for details
// about supported directives.
//
//= require jquery
//= require payola
//= require js/application
//= require jquery_ujs
//= require plugins/switchery/switchery.min
//= require plugins/navgoco/jquery.navgoco.min
//= require plugins/waypoints/waypoints.min
//= require jquery-ui/core
//= require jquery-ui/sortable
//= require dropzone.min
//= require plugins/countTo/jquery.countTo
//= require plugins/countTo/jquery.countTo
//= require plugins/weather/js/skycons
//= require plugins/morris/js/raphael.2.1.0.min
//= require plugins/morris/js/morris.min
//= require bootstrap
//= require jquery_nested_form
//= require signature-pad
//= require jquery.maskedinput.min
//= require bootstrap-datepicker
//= require modals
//= require bootstrap-tagsinput
//= require twitter/typeahead
//= require underscore
//= require gmaps/google
//= require typeahead.bundle
//= require typeahead-addresspicker
//= require nprogress
//= require nprogress-ajax 
//= require js/modernizr-2.6.2.min
//= require former
//= require common
