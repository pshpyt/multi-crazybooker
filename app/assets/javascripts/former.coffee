# Place all the behaviors and hooks related to the matching controller here.
# All this logic will automatically be available in application.js.
# You can use CoffeeScript in this file: http://coffeescript.org/

$(document).ready ->
  # disable auto discover
  $('.former-form .form-control').change ->
    field_value =  @value
    field_name = $(this).attr('name')
    record_id = $("#record_id").val()
    url = '/records/'.concat(record_id).concat('/update_field')
    
    $.ajax(
      method: 'GET'
      url: url
      data:
        id: record_id
        field_name: field_name
        field_value: field_value).done (msg) ->
      #$(this).append("saved")
      #$("#myElem").show().delay(5000).fadeOut();
      return
    return
  $('.former-form input:checkbox').change ->
    field_value =  @value
    field_name = $(this).attr('name')
    data = $("input[name='"+field_name+"']").serialize()
    field_type = $(this).attr('type')
    record_id = $("#record_id").val()
    url = '/records/'.concat(record_id).concat('/update_field?multi=true&').concat(data)
    
    $.ajax(
      method: 'GET'
      url: url
        ).done (msg) ->
      return
    return
  Dropzone.autoDiscover = false
  # grap our upload form by its id
  $('#new_attachment').dropzone
    maxFilesize: 5
    paramName: 'attachment[upfile]'
    addRemoveLinks: true
    success: (file, response) ->
      # find the remove button link of the uploaded file and give it an id
      # based of the fileID response from the server
      $(file.previewTemplate).find('.dz-remove').attr 'id', response.fileID
      # add the dz-success class (the green tick sign)
      $(file.previewElement).addClass 'dz-success'
      return
    removedfile: (file) ->
      # grap the id of the uploaded file we set earlier
      id = $(file.previewTemplate).find('.dz-remove').attr('id')
      # make a DELETE ajax request to delete the file
      $.ajax
        type: 'DELETE'
        url: '/attachments/' + id
        success: (data) ->
          console.log data.message
          return
      return
  return

