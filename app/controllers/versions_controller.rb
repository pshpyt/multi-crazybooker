class VersionsController < ApplicationController
  # before_action :require_user
  before_action :set_record_and_version, only: [:diff, :rollback, :destroy]

  def diff
  end

  def rollback
    # change the current record to the specified version
    # reify gives you the object of this version
    record = @version.reify
    record.save
    redirect_to record_path(record)
  end

  def bringback
    version = PaperTrail::Version.find(params[:id])
    @record = version.reify
    @record.save
    # Let's remove the version since the document is undeleted
    version.delete
    redirect_to records_path, notice: 'The record was successfully brought back!'
  end

  private

  def set_record_and_version
    @record = Record.find(params[:record_id])
    @version = @record.versions.find(params[:id])
  end
end
