class SupplementsController < ApplicationController
  before_action :set_supplement, only: [:show, :edit, :update, :destroy]

  # GET /supplements
  # GET /supplements.json
  def index
    @supplements = Supplement.all
  end

  # GET /supplements/1
  # GET /supplements/1.json
  def show
    @annotations = @supplement.get_annotations(request.original_url.gsub('.pdf', ''))
    respond_to do |format|
      format.html
      format.pdf { render_supplement_report(@supplement, @annotations) }
    end
  end

  # GET /supplements/new
  def new
    @supplement = Supplement.new
  end

  # GET /supplements/1/edit
  def edit
  end

  # POST /supplements
  # POST /supplements.json
  def create
    @supplement = Supplement.new(supplement_params)

    respond_to do |format|
      if @supplement.save
        FileWorker.perform_in(2.minutes, @supplement.id)
        format.html { redirect_to @supplement, notice: 'Supplement was successfully created.' }
        format.json { render :show, status: :created, location: @supplement }
      else
        format.html { render :new }
        format.json { render json: @supplement.errors, status: :unprocessable_entity }
      end
    end
  end

  # PATCH/PUT /supplements/1
  # PATCH/PUT /supplements/1.json
  def update
    respond_to do |format|
      if @supplement.update(supplement_params)
        format.html { redirect_to @supplement, notice: 'Supplement was successfully updated.' }
        format.json { render :show, status: :ok, location: @supplement }
      else
        format.html { render :edit }
        format.json { render json: @supplement.errors, status: :unprocessable_entity }
      end
    end
  end

  # DELETE /supplements/1
  # DELETE /supplements/1.json
  def destroy
    @supplement.destroy
    respond_to do |format|
      format.html { redirect_to supplements_url, notice: 'Supplement was successfully destroyed.' }
      format.json { head :no_content }
    end
  end

  private

  # Use callbacks to share common setup or constraints between actions.
  def set_supplement
    @supplement = Supplement.friendly.find(params[:id])
  end

  # Never trust parameters from the scary internet, only allow the white list through.
  def supplement_params
    params.require(:supplement).permit(:title, :slug, :description, :images_count, :source_pdf)
  end

  def render_supplement_report(supplement, annotations)
    report = ThinReports::Report.new
    report.use_layout "#{Rails.root}/app/reports/supplement_report", default: true
    report.use_layout "#{Rails.root}/app/reports/annotations", id: :ann_list
    supplement.images.each do |image|
      report.start_new_page do
        item(:title).value(supplement.title)
        list(:images).add_row do |row|
          row.item(:image).src(open(image.file.url))
        end
      end
    end
    report.start_new_page layout: :ann_list do
      annotations.each do |annotation|
        list(:annotations).add_row do |row|
          image = MiniMagick::Image.open(annotation['src'])
          dimension_height  = image.height
          dimension_width = image.width
          geometry = annotation['shapes'].first['geometry']
          geometry_string = "#{(geometry['width'] * dimension_width).to_i}x#{(geometry['height'] * dimension_height).to_i}+#{(geometry['x'] * dimension_width).to_i}+#{(geometry['y'] * dimension_height).to_i}"
          image = image.crop(geometry_string)
          file = File.open(image.path)
          row.item(:image).src(file)
          row.item(:annotation_text).value(annotation['text'])
        end
      end
    end
    send_data report.generate, filename: 'supplement_report.pdf',
                               type: 'application/pdf',
                               disposition: 'attachment'
  end
end
