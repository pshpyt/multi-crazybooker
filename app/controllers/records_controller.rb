require 'open-uri'
require 'open3'
class RecordsController < ApplicationController
  include TheSortableTreeController::ExpandNode
  include TheSortableTreeController::Rebuild
  load_and_authorize_resource param_method: :record_params, except: [:drafts, :approved, :awaiting_approvement]
  respond_to :html,  :json
  before_action do
  end
  before_action :authenticate_user!
  before_action :set_record, only: [:show, :edit, :update, :destroy, :sample_layout]

  def manage
    @records = Record.nested_set.select('id, title,  parent_id').all
  end

  def nested_options
    @records = Record.nested_set.select('id, title, parent_id').limit(15)
  end

  def node_manage
    @root = Record.root
    @records = @root.self_and_descendants.nested_set.select('id, title, parent_id').limit(15)
    render template: 'records/manage'
  end

  def expand
    @records = Record.nested_set.roots.select('id, title, parent_id')
  end

  # GET /records
  # GET /records.json
  def index
    @q = Record.ransack(params[:q])
    @records = @q.result.includes(:form, :user).page(params[:page])
  end

  def approved
    @q = Record.ransack(params[:q], approved: true)
    @records = @q.result.approved.includes(:form, :user).page(params[:page])
    render :index
  end

  def drafts
    @q = Record.ransack(params[:q], approved: true)
    @records = @q.result.drafts.includes(:form, :user).page(params[:page])
    render :index
  end

  def awaiting_approvement
    @q = Record.ransack(params[:q])
    @records = @q.result.awaiting_response.includes(:form, :user).page(params[:page])
    render :index
  end

  def accept_approvement
    record = Record.find(params[:record_id])
    record.update(status: 'approved')
    if request.xhr?
      render :json => {:status => 'ok'}
    else
      redirect_to approved_records_path
    end
  end

  def cancel_approvement
    record = Record.find(params[:record_id])
    record.update(status: 'draft')
    if request.xhr?
      render :json => {:status => 'ok'}
    else
      redirect_to drafts_records_path
    end
  end

  def approvement_request
    record = Record.find(params[:record_id])
    record.update(status: 'awaiting')
    redirect_to awaiting_approvement_records_path
  end

  # GET /records/1
  # GET /records/1.json
  def show
    respond_to do |format|
      format.html
      format.pdf { render_record_report(@record) }
    end
  end
  
  # GET /records/new
  def new
    @record = Record.new
    @record.data = {}
    if params[:form_id]
      @form = Form.find(params[:form_id])
    else
      @form = Form.first
    end
    @record.master_record_id = params[:master_record_id] if params[:master_record_id]
    @record.form = @form
    respond_modal_with @record
  end

  # GET /records/1/edit
  def edit
    @form = Form.all[0]
    add_breadcrumb "##{@record.id}", record_path(@record) # , :only => [:edit, :view, :new]
    add_breadcrumb 'Edit', edit_record_path(@record)
  end

  def create_record
    if params.key?(:account_id)
      account = Account.find(params[:account_id])
      Apartment::Tenant.switch(account.subdomain)
    end
    @record = current_user.records.create(form_id: params[:form_id])
    @record.attachments.new
    redirect_to former_path(:step1, record_id: @record.id)
  end

  # POST /records
  # POST /records.json
  def create
    @record = current_user.records.create(record_params)
    respond_modal_with @record, location: request.referer
  end

  # PATCH/PUT /records/1
  # PATCH/PUT /records/1.json
  def update
    if params[:record][:admin_signature]
      instructions = JSON.parse(params[:record][:admin_signature]).map { |h| "line #{h['mx'].to_i},#{h['my'].to_i} #{h['lx'].to_i},#{h['ly'].to_i}" } * ' '
      tempfile = Tempfile.new(['signature', '.png'])
      Open3.popen3("convert -size 698x300 xc:transparent -stroke blue -draw @- #{tempfile.path}") do |input, _output, _error|
        input.puts instructions
      end
      @record.admin_signature = tempfile
      record_params.delete 'admin_signature'
    end

    respond_to do |format|
      if @record.update(record_params)
        format.html { redirect_to records_url, notice: 'Record was successfully updated.' }
        format.json { render :show, status: :ok, location: @record }
      else
        format.html { render :edit }
        format.json { render json: @record.errors, status: :unprocessable_entity }
      end
    end
  end

  # DELETE /records/1
  # DELETE /records/1.json
  def destroy
    @record.destroy
    respond_to do |format|
      format.html { redirect_to request.referer, notice: 'Record was successfully destroyed.' }
      format.json { head :no_content }
    end
  end

  def deleted
    @records = PaperTrail::Version.where(event: 'destroy')
  end

  def update_field
    record = Record.find(params[:id])
    if params.key?('multi')
      params['record']['data'].each do |key, value|
        record.data[key] = value.to_json
      end
    else
      field_name = params[:field_name].gsub('record[data]', '').delete('[]')
      field_value = params[:field_value]
      record.data[field_name] = field_value
    end
    record.save
    render json: { msg: 'field updated', status: 'whatever' }
  end

  def import_csv
    file = params[:csv_file]
    total = current_user.populate_records(file.path, params[:form_id])

    respond_to do |format|
      format.html { redirect_to records_url, notice: "File was successfully parsed. (#{total} records added)" }
      format.json { head :no_content }
    end
 end

  def history
    @versions = PaperTrail::Version.order('created_at DESC')
 end

  private

  def render_record_report(record)
    send_data record.report.generate, filename: 'report.pdf',
                               type: 'application/pdf',
                               disposition: 'attachment'
  end
  
  # Use callbacks to share common setup or constraints between actions.
  def set_record
    @record = Record.find(params[:id])
  end

  # Never trust parameters from the scary internet, only allow the white list through.
  def record_params
    # params.require(:record).permit(:form_id, :data)
    params.require(:record).permit!
  end
    end
