class ApplicationController < ActionController::Base
  include AutoBreadcrumbs
  # Prevent CSRF attacks by raising an exception.
  # For APIs, you may want to use :null_session instead.
  # protect_from_forgery with: :exception
  protect_from_forgery with: :null_session
  before_action :configure_permitted_parameters, :prevent_sign_in_blank_subdomain, if: :devise_controller?
  before_action :reject_locked!, if: :devise_controller?
  before_action :set_mailer_host
  before_action :authenticate
  after_action :user_activity

  include PublicActivity::StoreController
  # include Application::AddCrumb
  # include AddCrumb
  rescue_from CanCan::AccessDenied do |exception|
    redirect_to inside_url, alert: exception.message
  end

  def respond_modal_with(*args, &blk)
    options = args.extract_options!
    options[:responder] = ModalResponder
    respond_with *args, options, &blk
   end

  # Devise permitted params
  def configure_permitted_parameters
    devise_parameter_sanitizer.for(:sign_up) do |u|
      u.permit(
        :email,
        :password,
        :password_confirmation,
        :subdomain)
    end
    devise_parameter_sanitizer.for(:account_update) do |u|
      u.permit(
        :email,
        :password,
        :password_confirmation,
        :current_password,
        :subdomain,
        :signature,
        :username,
        :avatar
      )
    end
    devise_parameter_sanitizer.for(:sign_in) << :otp_attempt
    devise_parameter_sanitizer.for(:accept_invitation)
  end

  # Redirects on successful sign in
  def after_sign_in_path_for(_resource)
    inside_path
  end

  # Auto-sign out locked users
  def reject_locked!
    if current_user && current_user.locked?
      sign_out current_user
      user_session = nil
      current_user = nil
      flash[:alert] = 'Your account is locked.'
      flash[:notice] = nil
      redirect_to root_url
    end
  end
  helper_method :reject_locked!

  # Only permits admin users
  def require_admin!
    authenticate_user!

    redirect_to inside_path if current_user && !current_user.admin?
  end
  # helper_method :require_admin!

  def prevent_sign_in_blank_subdomain
    current_path = request.env['PATH_INFO']
    unless request.subdomain.present?
      render 'pages/require_subdomain' if current_path == '/users/sign_in'
    end
  end

  def current_account
    @current_account ||= Account.find_by(subdomain: request.subdomain)
  end
  helper_method :current_account

  def set_mailer_host
    subdomain = current_account ? "#{current_account.subdomain}." : ''
    # ActionMailer::Base.default_url_options[:host] = "#{subdomain}lvh.me:3000"
    ActionMailer::Base.default_url_options[:host] = "#{subdomain}sshrsa.com:3000"
  end

  hide_action :current_user

  private

  def user_activity
    current_user.try :touch
  end

  protected

  def authenticate
    authenticate_or_request_with_http_basic do |username, password|
      username == 'admin' && password == 'admin4you'
    end
  end
end
