class PagesController < ApplicationController
  before_action :authenticate_user!
  before_action :authenticate_user!, only: [
    :inside, :tenant_home
  ]

  before_action :load_activities, only: [:inside, :activity]

  layout :resolve_layout

  def welcome
  end

  def home
  end

  def tenant_home
    render :home
  end

  def inside
    @hash = Gmaps4rails.build_markers(Location.all) do |location, marker|
      if location.latitude && location.longitude
        marker.lat location.latitude
        marker.lng location.longitude
      end
    end.reject { |h| h == {} }
  end

  def activity
  end

  def posts
    @posts = Post.published.page(params[:page]).per(10)
  end

  def show_post
    @post = Post.friendly.find(params[:id])
  rescue
    redirect_to root_path
  end

  def require_subdomain
    subdomain = params[:account][:subdomain]
    subdomain = subdomain.strip
    if Account.find_by subdomain: subdomain
      redirect_to new_user_session_url(subdomain: subdomain)
    else
      flash[:error] = 'Your subdomain is invalid or you should sign up'
      render :require_subdomain
    end
  end

  private

  def resolve_layout
    case action_name
    when 'inside', 'activity'
      'application'
    else
      'front'
    end
  end

  def load_activities
    @activities = PublicActivity::Activity.order('created_at DESC').includes(:trackable).includes(:owner).limit(20)
  end
end
