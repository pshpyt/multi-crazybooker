require 'open3'
class FormerController < ApplicationController
  before_action :authenticate_user!

  include Wicked::Wizard
  before_action :set_steps
  before_action :setup_wizard
  before_action :set_record_id

  def show
    @record = Record.includes(:form).find(params[:record_id])
    @form = @record.form
    if step != :finish
      @step_object = @form.steps.where(number: step.to_s.gsub('step', '').to_i).first
    end
    render_wizard
  end

  def update
    @record = Record.includes(:user).find(params[:record][:id])

    current_step_index = (wizard_steps.index(step) + 1)
    if @record.extras
      @record.extras['current_step'] = next_step
      @record.extras['progress'] = "#{current_step_index.to_f.send(:/, wizard_steps.size).send(:*, 100).round(0)}%"
    else
      @record.extras = { 'current_step' => next_step, 'progress' => "#{(current_step_index).to_f.send(:/, wizard_steps.size).send(:*, 100)}%" }
    end

    if params[:record][:signature]
      instructions = JSON.parse(params[:record][:signature]).map { |h| "line #{h['mx'].to_i},#{h['my'].to_i} #{h['lx'].to_i},#{h['ly'].to_i}" } * ' '
      tempfile = Tempfile.new(['signature', '.png'])
      Open3.popen3("convert -size 698x300 xc:transparent -stroke blue -draw @- #{tempfile.path}") do |input, _output, _error|
        input.puts instructions
      end
      @record.signature = tempfile
    end
    if @record.data
      @record.data = @record.data.merge(params[:record][:data]) if params[:record][:data]
    else
      @record.data = params[:record][:data] if params[:record][:data]
    end
    @record.save
    if params[:record][:relations_attributes] || params[:record][:locations_attributes]
      @record.update(record_params)
    end
    redirect_to wizard_path(@next_step, record_id: @record.id)
  end

  def finish_wizard_path
  end

  private

  def redirect_to_finish_wizard
    redirect_to root_url, notice: 'Thanks for signing up.'
  end

  def set_record_id
    session[:record_id] = params[:record_id] if params[:record_id]
  end

  def set_steps
    session[:record_id] = params[:record_id] if params[:record_id]
    @form = Record.find(session[:record_id]).form
    form_steps = @form.steps.map { |s| "step#{s.number}" }
    form_steps << 'signature' if @form.signature_required
    form_steps << 'attachments' if @form.file_upload
    form_steps << 'finish'
    self.steps = form_steps
  end

  def record_params
    params.require(:record).permit!
  end
end
