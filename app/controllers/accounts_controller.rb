class AccountsController < ApplicationController
  skip_before_filter :authenticate_user!, only: [:new, :create]
  include Payola::StatusBehavior
  before_action :cancel_subscription, only: [:destroy]
  layout :resolve_layout
  
  def new
    @account = Account.new
    @account.build_owner    
    unless params[:plan].nil?
      @plan = Plan.find_by!(stripe_id: params[:plan])
      @account.owner.plan = @plan
    end
  end

  def create
    Apartment::Tenant.switch('public')
    @account = Account.new(account_params)
    @account.owner.admin = true        
    
    if @account.valid?      
      plan = Plan.find_by!(id: params[:account][:owner_attributes][:plan_id].to_i)      
      @account.owner.role = User.roles[plan.stripe_id]
      Apartment::Tenant.create(@account.subdomain)      
      Apartment::Tenant.switch(@account.subdomain)
      CreatePlanService.new.call
      @account.save
      Apartment::Tenant.switch('public')
      subscribe
    else
        render json:
          {error: @account.errors.full_messages.to_sentence},
               status: 400
      #render action: 'new'
    end
  end

  def change_plan
  
  end

  private
  def account_params
    params.require(:account).permit(:subdomain, owner_attributes: [:email, :password, :password_confirmation, :admin, :plan_id])
  end

  def subscribe               
    owner = @account.owner
    params[:plan] = owner.plan
    subscription = Payola::CreateSubscription.call(params, owner)
    owner.save
    render_payola_status(subscription)
  end

  def cancel_subscription
    subscription = Payola::Subscription.find_by!(owner_id: @owner.id, state: 'active')
    Payola::CancelSubscription.call(subscription)
  end

  def resolve_layout
    case action_name
    when "new"
      "devise"
    else
      "application"
    end
  end
end
