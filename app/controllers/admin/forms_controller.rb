class Admin::FormsController < Admin::BaseController
  protect_from_forgery with: :null_session, if: proc { |c| c.request.format == 'application/json' }
  respond_to :html, :json
  before_action :set_form, only: [
    :edit,
    :update,
    :destroy,
    :show
  ]

  def dashboard
    @published_form_count = Form.count
  end

  def index
    # @forms = Form.where('account_id=?', current_account.id).page(params[:page]).per(50)
    @q = Form.where('account_id=?', current_account.id).ransack(params[:q])
    @forms = @q.result.page(params[:page])
    # @forms = Form.page(params[:page]).per(50)
  end

  def drafts
    @forms = Form.page(params[:page]).per(50)
  end

  def new
    @form = Form.new
    respond_modal_with @form
  end
  
  def show
    respond_to do |format|
      format.tlf { render_sample_tlf(@form) }
    end
  end

  def create
    @form = Form.new(form_params)
    @form.account = current_account
    @form.save
    respond_modal_with @form, location: admin_forms_path
  end

  def edit
  end

  def update
    
    @form.slug = nil
    if @form.update(form_params)
      binding.pry
      redirect_to admin_forms_path, notice: 'Form successfully edited.'
    else
      flash[:alert] = 'The form was not edited.'
      render :edit
    end
  end

  def destroy
    @form.destroy
    redirect_to admin_forms_path, notice: 'The form has been deleted.'
  end

  private

  def set_form
    @form = Form.friendly.find(params[:id])
    redirect_to action: 'index' if @form.account_id != current_account.id
  end
  
  def render_sample_tlf(form)
    send_data File.read(form.sample_layout), filename: 'sample.tlf',
                               type: 'application/tlf',
                               disposition: 'attachment'
  end

  def form_params
    params.require(:form).permit!
    # params.require(:form).permit(
    # :name,
    # :description,
    # :content_md,
    # :draft,
    # :updated_at,
    # inputs_attributes: [:id, :name, :description, :required, :_destroy],
    # steps_attributes: [:id, :title, :description, :number, :_destroy]
    # )
  end
end
