class ApplicationMailer < ActionMailer::Base
  default from: "multibooker@gmail.com"
  layout 'mailer'
end
