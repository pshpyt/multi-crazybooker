class UserMailer < ApplicationMailer
	default :from => "multibooker@gmail.com"

  def expire_email(user)
    mail(:to => user.email, :subject => "Subscription Cancelled")
  end
end
