class Relation < ActiveRecord::Base
  belongs_to :record, foreign_key: 'record_id', class_name: 'Record'
  belongs_to :related_record, foreign_key: 'related_record_id', class_name: 'Record'
end
