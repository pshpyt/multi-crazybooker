require 'rmagick'
class Supplement < ActiveRecord::Base
  # after_create :split_to_images

  has_many :images
  has_attached_file :source_pdf,
                    storage: :dropbox,
                    dropbox_credentials: Rails.root.join('config/dropbox.yml'),
                    dropbox_options: {}

  do_not_validate_attachment_file_type :source_pdf
  extend FriendlyId
  friendly_id :title, use: :slugged
  # private

  def split_to_images
    source_pdf_url = source_pdf.url
    pdf = Magick::ImageList.new(source_pdf_url)
    pdf.each_with_index do |magick_image, index|
      begin
        file = Tempfile.create ["_#{index}_", '.jpg']
        magick_image.write(file.path)
        images.create(file: file)
      end
    end
    end

  def get_annotations(original_url)
    client = Parse.create(application_id: 'TwYltjxRhqIs9Cdv7dV4kQD1iiNzUPhitFLoTThh',
                          api_key: 'D3OdPMjEQDcY9801vV89AfHGphbHeTQzLIXj2PeK')
    annotations = client.query('Annotation').eq('context', original_url).get
    annotations
  end
end
