class Attachment < ActiveRecord::Base
  belongs_to :record
  has_attached_file :upfile,
                    storage: :dropbox,
                    dropbox_credentials: Rails.root.join('config/dropbox.yml'),
                    dropbox_options: {}
  validates_attachment :upfile, content_type: { content_type: ['image/jpg', 'image/jpeg', 'image/png', 'image/gif'] }
end
