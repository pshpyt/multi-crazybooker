class Plan < ActiveRecord::Base
  include Payola::Plan
  include Rails.application.routes.url_helpers
  include UrlHelper

  has_many :users

  def redirect_path(_subscription)
    account = Account.order('created_at').last
    root_url(subdomain: account.subdomain)
  end
end
