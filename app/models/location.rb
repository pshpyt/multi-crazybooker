class Location < ActiveRecord::Base
  belongs_to :mapable, polymorphic: true
  geocoded_by :full_address # can also be an IP address
  after_validation :geocode
end
