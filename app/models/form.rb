class Form < ActiveRecord::Base
  has_many :slave_forms, class_name: 'Form', foreign_key: 'master_form_id'

  belongs_to :master_form, class_name: 'Form'
  has_attached_file :template,
                    storage: :dropbox,
                    dropbox_credentials: Rails.root.join('config/dropbox.yml'),
                    dropbox_options: {}
  # validates_attachment_content_type :template, :content_type => 'text/plain'
  do_not_validate_attachment_file_type :template

  default_scope { order('created_at ASC') }
  scope :master, -> { where(master_form_id: nil) }
  # Use friendly_id
  extend FriendlyId
  friendly_id :name, use: :slugged

  # Markdown
  # before_save { MarkdownWriter.update_html(self) }

  # Validations
  validates :name, presence: true, length: { maximum: 100 }, uniqueness: true
  # validates :content_md, presence: true

  # Pagination
  paginates_per 30

  # Relations
  has_many :inputs
  has_many :records
  has_many :steps
  belongs_to :account

  accepts_nested_attributes_for :inputs, allow_destroy: true
  accepts_nested_attributes_for :steps, allow_destroy: true
  accepts_nested_attributes_for :slave_forms, allow_destroy: true

  scope :master, -> { where(master_form_id: nil) }

  def friendly_headers
    friendly_headers = []
    steps.each do |step|
      step.inputs.each do |input|
        friendly_headers << input.friendly_id
      end
    end
    friendly_headers
  end
  
  def column_headers
    column_headers = []
    steps.each do |step|
      step.inputs.each do |input|
        column_headers << input.name
      end
    end
    column_headers
  end

  def column_headers_slave
    column_headers = []
    inputs.each do |input|
      column_headers << input.name
    end
    column_headers
  end
  
  def sample_layout
    sample_tlf = TlfParser.new(friendly_headers)
    sample_tlf.result
  end

end
