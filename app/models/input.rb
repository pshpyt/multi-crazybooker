class Input < ActiveRecord::Base
  default_scope { order('created_at ASC') }
  belongs_to :form
  belongs_to :step
  has_many :field_values
  extend FriendlyId
  friendly_id :name_and_uuid
  def name_and_uuid
    "#{name} #{SecureRandom.hex(7)}"
  end

  CONTROL_TYPES = [
    { 'name' => 'Text Field', 'value' => 'text_field' },
    { 'name' => 'Text Area',  'value' => 'text_area' },
    { 'name' => 'Dropdown',   'value' => 'select' },
    { 'name' => 'Radio',      'value' => 'radio' },
    { 'name' => 'Date', 'value' => 'date' },
    { 'name' => 'Phone',      'value' => 'phone' },
    { 'name' => 'Email',      'value' => 'email' },
    { 'name' => 'Checkboxes', 'value' => 'checkboxes' },
    { 'name' => 'Tags', 'value' => 'tags' },
    { 'name' => 'Location', 'value' => 'location' },
    { 'name' => 'Relation', 'value' => 'relation' }
  ]

  accepts_nested_attributes_for :field_values, allow_destroy: true
end
