class Step < ActiveRecord::Base
  default_scope { order('created_at ASC') }
  belongs_to :form
  has_many :inputs
  has_many :slave_forms, class_name: 'Form', foreign_key: 'master_form_id'
  has_many :locations, as: :mapable
  accepts_nested_attributes_for :inputs, allow_destroy: true
  accepts_nested_attributes_for :slave_forms, allow_destroy: true
end
