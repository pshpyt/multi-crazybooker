class FieldValue < ActiveRecord::Base
  belongs_to :input
  default_scope { order('created_at DESC') }
end
