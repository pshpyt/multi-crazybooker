require 'rmagick'
class Image < ActiveRecord::Base
  belongs_to :supplement, counter_cache: true
  has_attached_file :file,
                    storage: :dropbox,
                    dropbox_credentials: Rails.root.join('config/dropbox.yml'),
                    dropbox_options: {}

  validates_attachment_content_type :file, content_type: /\Aimage/
  # Validate filename
  validates_attachment_file_name :file, matches: [/png\Z/, /jpe?g\Z/]
  # do_not_validate_attachment_file_type :file
end
