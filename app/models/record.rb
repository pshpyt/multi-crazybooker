class Record < ActiveRecord::Base
  include Rails.application.routes.url_helpers
  # default_scope { order('created_at DESC') }
  before_create :add_data
  scope :by_form, -> (form) { where(form_id: form) }
  has_many :locations, as: :mapable
  acts_as_nested_set order_column: :lft
  include TheSortableTree::Scopes

  store_accessor :data

  has_attached_file :signature,
                    storage: :dropbox,
                    dropbox_credentials: Rails.root.join('config/dropbox.yml'),
                    dropbox_options: {}

  has_attached_file :admin_signature,
                    storage: :dropbox,
                    dropbox_credentials: Rails.root.join('config/dropbox.yml'),
                    dropbox_options: {}
  validates_attachment :signature, 
                content_type: { content_type: ['image/jpg', 'image/jpeg', 'image/png', 'image/gif'] },
                :default_url => "/image-not-available"
  validates_attachment :admin_signature, content_type: { content_type: ['image/jpg', 'image/jpeg', 'image/png', 'image/gif'] }

  RECORD_STATUSES = %w(draft awaiting approved)

  scope :drafts, -> { where(master_record_id: nil, status: 'draft') }
  scope :awaiting_response, -> { where(master_record_id: nil, status: 'awaiting') }
  scope :approved, -> { where(master_record_id: nil, status: 'approved') }

  has_many :relations, foreign_key: 'record_id', class_name: 'Relation'
  has_many :related_records, through: :relations
  has_many :slave_records, class_name: 'Record', foreign_key: 'master_record_id'
  belongs_to :master_record, class_name: 'Record'

  belongs_to :form
  counter_culture :form
  counter_culture [:form, :account]
  belongs_to :user
  has_many :attachments, dependent: :destroy

  accepts_nested_attributes_for :attachments, allow_destroy: true
  accepts_nested_attributes_for :relations, allow_destroy: true
  accepts_nested_attributes_for :related_records, allow_destroy: true
  accepts_nested_attributes_for :locations, allow_destroy: true

  has_paper_trail only: [:data, :status]

  include PublicActivity::Model
  tracked owner: proc { |controller, _model| controller.current_user ? controller.current_user : nil }
  # return array of data hashes
  def data_fields
    data.to_hash
  rescue
    {}
  end
 
  def public_title
    "#{id} #{form.name}"
  end
  
  def load_layout
    record = self
    temp_layout = Tempfile.new(['layout', '.tlf'])
    template_url = record.form.template.url
    remote_data = open(template_url).read
    temp_layout.write(remote_data)
    temp_layout.close
    temp_layout
  end
  
  def report
    record = self
    temp_layout = Tempfile.new(['layout', '.tlf'])
    template_url = record.form.template.url
    remote_data = open(template_url).read
    temp_layout.write(remote_data)
    temp_layout.close
    qrcode = Tempfile.new(['qrcode', '.png'])
    qrcode.write(RQRCode::QRCode.new(edit_record_path(self)).as_png)
    qrcode.close
    report = ThinReports::Report.new layout: temp_layout.path
      report.start_new_page do
        item(:record_id).value(record.id) rescue nil  
        if record.signature.exists?
          item(:signature).src(open(record.signature.url)) rescue nil
        end
        item(:qr_code).src(qrcode.path) rescue nil
        record.data.each_pair do |key,value|  
            item(key).value(value) rescue nil
        end
        record.slave_records.each do |child|
          list.add_row do |row|
            row.item(:id).value(child.id) rescue nil
            row.item(:data).value(child.data.to_s) rescue nil
            child.data.each_pair do |key,value|  
              row.item(key).value(value) rescue nil
            end
          end
        end
    end
    report
  end
   
  private

  def add_data
    self.data = Hash.new unless data
    self.status = 'draft'
  end

  def self.ransackable_scopes(_auth_object = nil)
    [:drafts, :awaiting_response, :approved]
  end
  end
