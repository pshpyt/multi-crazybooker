class User < ActiveRecord::Base
  include PublicActivity::Model
  tracked only: [], owner: proc { |controller, _model| controller.current_user ? controller.current_user : nil }

  include DeviseInvitable::Inviter
  devise :two_factor_authenticatable,
         otp_secret_encryption_key: 'ENV[DEVISE_ENCRYPT_KEY]'

  has_attached_file :signature,
                    storage: :dropbox,
                    dropbox_credentials: Rails.root.join('config/dropbox.yml'),
                    dropbox_options: {}

  validates_attachment :signature, content_type: { content_type: ['image/jpg', 'image/jpeg', 'image/png', 'image/gif'] }

  has_attached_file :avatar,
                    storage: :dropbox,
                    dropbox_credentials: Rails.root.join('config/dropbox.yml'),
                    dropbox_options: {},
                    styles: { medium: '300x300>', thumb: '50x50#' }, default_url: '/images/:style/missing.png'

  validates_attachment_content_type :avatar, content_type: /\Aimage\/.*\Z/

  enum role: [:user, :silver, :gold, :platinum]
  after_initialize :set_default_role, if: :new_record?
  after_initialize :set_default_plan, if: :new_record?

  # Relations
  has_many :posts
  has_many :records, dependent: :destroy
  belongs_to :plan

  # Include default devise modules. Others available are:
  # :confirmable, :lockable, :timeoutable and :omniauthable
  devise :invitable, :registerable,
         :recoverable, :rememberable, :trackable, :validatable # , :confirmable

  # Pagination
  paginates_per 100

  default_scope { order('id ASC') }
  scope :online, -> { where('updated_at > ?', 10.minutes.ago) }
  # Validations
  # :email
  validates_format_of :email, with: /\A([^@\s]+)@((?:[-a-z0-9]+\.)+[a-z]{2,})\Z/i
  validates_associated :plan

  def after_database_authentication
    create_activity :logged_in, parameters: { forename: forename, current_sign_in_ip: current_sign_in_ip, current_sign_in_at: current_sign_in_at }
    # self.update_attribute(:invite_code, nil)
  end
  # added by louis to implement multitenant
  # before_save :default_save

  def self.find_version_author(version)
    find(version.terminator)
  end

  def self.paged(page_number)
    order(admin: :desc, email: :asc).page page_number
  end

  def self.search_and_order(search, page_number)
    if search
      where('email LIKE ?', "%#{search.downcase}%").order(
        admin: :desc, email: :asc
      ).page page_number
    else
      order(admin: :desc, email: :asc).page page_number
    end
  end

  def self.last_signups(count)
    order(created_at: :desc).limit(count).select('id', 'email', 'created_at')
  end

  def self.last_signins(count)
    order(last_sign_in_at:
    :desc).limit(count).select('id', 'email', 'last_sign_in_at')
  end

  def self.users_count
    where('admin = ? AND locked = ?', false, false).count
  end

  def set_default_role
    self.admin = false
    self.role ||= :user
  end

  def set_default_plan
    self.plan ||= Plan.last
  end

  def populate_records(file_path, form_id)
    rows = SmarterCSV.process(file_path, options = { row_sep: :auto })
    rows.each do |row|
      records.create(form_id: form_id, data: row)
    end
    rows.size
  end

  def online?
    updated_at > 10.minutes.ago
  end

  def forename
    username.blank? ? email : username
  end
end
