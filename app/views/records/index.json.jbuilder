json.array!(@records) do |record|
  json.extract! record, :id, :form_id, :data
  json.url record_url(record, format: :json)
end
