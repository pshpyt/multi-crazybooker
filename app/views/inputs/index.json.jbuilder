json.array!(@inputs) do |input|
  json.extract! input, :id, :name, :description, :required, :form_id
  json.url input_url(input, format: :json)
end
