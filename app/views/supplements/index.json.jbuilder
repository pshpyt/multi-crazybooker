json.array!(@supplements) do |supplement|
  json.extract! supplement, :id, :title, :slug, :description, :images_count
  json.url supplement_url(supplement, format: :json)
end
