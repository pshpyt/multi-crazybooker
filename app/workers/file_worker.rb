class FileWorker
  include Sidekiq::Worker
  def perform(supplement_id)
    # do something
  	Supplement.find(supplement_id).split_to_images
  end
end
