module ApplicationHelper
  def title(value)
    unless value.nil?
      @title = "#{value} | Booker"
    end
  end
  
  
 # For generating time tags calculated using jquery.timeago
  def timeago(time, options = {})
    options[:class] ||= "timeago"
    content_tag(:abbr, time.to_s, options.merge(:title => time.getutc.iso8601)) if time
  end

  # Shortcut for outputing proper ownership of objects,
  # depending on who is looking
  def whose?(user, object)
    case object
      when Post
        owner = object.author
      when Comment
        owner = object.user
      else
        owner = nil
    end
    if user and owner
      if user.id == owner.id
        "his"
      else
        "#{owner.nickname}'s"
      end
    else
      ""
    end
  end
  # Check if object still exists in the database and display a link to it,
  # otherwise display a proper message about it.
  # This is used in activities that can refer to
  # objects which no longer exist, like removed posts.
  def link_to_trackable(object, object_type)
    if object
      link_to object_type.downcase, object
    else
      "a #{object_type.downcase} which does not exist anymore"
    end
  end
  
  def record_status(status)
    case status
    when 'awaiting'
      content_tag(:span, class: "label label-warning") do
        status
      end
    when 'approved'
      content_tag(:span, class: "label label-success") do
        status
      end
    else
      content_tag(:span, class: "label label-info") do
        status
      end
    end
  end

  def tutorial_progress_bar
    #content_tag(:section, class: "content1") do
      #content_tag(:div, class: "navigator1") do
    step_number = 0
      content_tag(:div, class: "wizard", id: "step-wizard") do
        #content_tag(:ul, class: "list-group") do
        step_list = content_tag(:ul, class: "steps") do
          wizard_steps.collect do |every_step|
            step_number = step_number + 1
            data_target = "##{every_step}"
            class_str = "#{every_step} active"  if every_step == step
            step1_class = "badge-info"  if every_step == step
            #class_str = "list-group-item list-group-item-success" if past_step?(every_step)
            concat(
              content_tag(:li, class: class_str, 'data-target': data_target) do
                #link_to every_step, wizard_path(every_step)
                #{}"#{every_step}"
                step1_class = 'badge-info' if step_number == 1
                concat(content_tag(:span, step_number, class: "badge #{step1_class}"))
                concat("#{every_step}")
                concat(content_tag(:span, '', class: "chevron"))
              end 
            )   
          end 
        end

        action_buttons = content_tag(:div, class: 'actions') do
          concat(link_to('< Prev', "#{previous_wizard_path}" + "?record_id=#{@record.id}", class: "btn btn-default btn-mini btn-prev", type: "button"))
          if next_wizard_path == "/former/wicked_finish"
            concat(submit_tag('Finished', class: "btn btn-primary btn-mini btn-next", :disabled=>true))
          else
            concat(submit_tag('Next >', class: "btn btn-primary btn-mini btn-next"))
          end
        end        

=begin
        action_buttons = content_tag(:div, class: "actions") do
          concat(
            content_tag(:button, class: "btn btn-default btn-mini btn-prev", type: "button") do
              concat(content_tag(:i, '', class: "fa fa-chevron-left"))
              concat('Prev')
            end
          )

          concat(
            content_tag(:button,  'data-last': "Finish", class: "btn btn-primary btn-mini btn-next", type: "button") do
              concat('Next')
              concat(content_tag(:i, '', class: "fa fa-chevron-right"))              
            end
          )
        end
=end
        step_list + action_buttons
      end
    #end
  end 
end
