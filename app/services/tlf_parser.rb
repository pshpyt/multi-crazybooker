class TlfParser
  TEXT_PARTIAL_PATH = "#{Rails.root}/public/tlf_text_partial.tlf"
  #LAYOUT_PATH       = "#{Rails.root}/public/tlf_layout3.tlf" 
  LAYOUT_PATH       = "#{Rails.root}/public/header_and_bottom.tlf" 
  #LIST_PARTIAL_PATH       = "#{Rails.root}/public/list_partial" 
  #LIST_PARTIAL_PATH       = "#{Rails.root}/public/nested_partial_x.txt" 
  
  def initialize(*args)
    @fields = args.flatten
  end
  
  def fields
    @fields
  end
  
 #def replace(filepath,newfile, regexp, *args, &block)
 #  content = File.read(filepath).gsub(regexp, *args, &block)
 #  partial = File.read(TEXT_PARTIAL_PATH)
 #
 #  File.open(newfile, 'wb') { |file| file.write(content) }
 #end
  
  def get_layout
    File.read(LAYOUT_PATH)
  end
  
  def get_text_partial
    File.read(TEXT_PARTIAL_PATH)
  end
  
  def get_list_partial
    File.read(LIST_PARTIAL_PATH)
  end
  
  def generate_list
    get_list_partial
  end

  def generate_body
    partial = get_text_partial
    body = ''
    x = 150
    y = 50

    fields.each do |field_name|
      body << partial.gsub('attribute_to_replace', field_name)
      .gsub('y_position',(y).to_s)
      .gsub('x_position',x.to_s).chomp
      .gsub('y2_position',(y+10).to_s).chomp
      .gsub('x2_position',(x+10).to_s).chomp
      .gsub('label_x',(x-100).to_s).chomp
      .gsub('label_y',(y).to_s).chomp
      .gsub('label_to_replace',field_name.split('_').first).chomp
      
      y += 30
    end
    body
  end

  def result
    layout_content = get_layout
    #list = generate_list
    body = generate_body
    #content = layout_content.gsub('place_to_replace', body)#.gsub('list', list)
    content = layout_content.gsub('pashexalex', body)
    #newfile = "#{Rails.root}/public/result.tlf"
    sample_file = Tempfile.new(['sample_layout', '.tlf'])
    File.open(sample_file, 'wb') { |file| file.write(content) }
    sample_file
  end
end
