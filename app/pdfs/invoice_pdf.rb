class InvoicePdf < Prawn::Document

  def initialize(record, view)
    super()
    @record = record
    @view = view
    header_message
   
  table( record_rows , :row_colors => ["FFFFFF","DDDDDD"], :cell_style => {:padding => [2, 2, 2, 30]}, :width => 500)
  sign_section  
  date_message
  end

  def record_rows
    arr = []
    arr << ["Field","Value"]
    if @record.data
      @record.form.steps.each do |step|
        step.inputs.each do |input|
          arr << [input.name, @record.data["#{input.name}"]]
        end
      end
    end
     #@record.data.each_pair do |key, value|
       # arr << [key, value]
    #end
  arr
  end
  
  def sign_section
    if @record.signature.url != "/signatures/original/missing.png"
     image  open(@record.signature.url), :scale => 0.6   
    end
  end
  
  def subscription_item_rows
    [["Description", "Quantity", "Rate", "Amount"]] +
      
      [ "asdasdasda",  "asdasdasda",  " sdfsdfsdfs", "ddddddddddd" ]
  end

  def precision(num)
    @view.number_with_precision(num, :precision => 2)
  end

  def header_message
    move_down 30
    text "#{@record.form.name} ##{@record.id}",
    :indent_paragraphs => 170, :size => 14, style:  :bold
    move_down 30
  end
  
  def date_message
    move_down 30
    text "Date report generated: #{@record.created_at.to_date.to_s}",
    :indent_paragraphs => 250, :size => 12, style:  :bold
  end
end
